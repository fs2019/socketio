const express = require('express')
const app = express()
const http = require('http').Server(app)
const io = require('socket.io')(http);

app.get('/', (req, res) => res.sendFile('./index.html', {root: __dirname}))

// basic usage
io.on('connection', (socket) => {
  // each socket is associated to a specific client
  console.log(`client connected: ${socket.id}`)

  // triggers when a client connects
  io.clients((err, clients) => {
    // broadcasts to all connected clients  
    io.emit('clients', clients)
  })

  // when on a client : socket.send('stuff') or socket.emit('message', 'stuff')
  socket.on('message', console.log)
  socket.on('other', console.log)

  // broadcasts to all client BUT this one
  socket.on('from_one_to_other', data => {
    socket.broadcast.emit('broadcast', `sent from ${socket.id}: ${data}`)
  })


  // room : limit broadcast space
  socket.on('join', room => socket.join(room))
  socket.on('leave', room => socket.leave(room))
  socket.on('broadcast', (room, msg) => socket.to(room).send(msg))

})

http.listen(process.env.PORT || 3000)
